Version 17.0.0.1 : (08-02-2024)
	- pos config and res config settings field name is same change field name.

Version 17.0.0.2 : (28/03/2024)
	- Change code for barcode print in xml.

Version 17.0.0.3 : (12/04/2024)
	- Fixed issue related to close screen.
