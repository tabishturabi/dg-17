/** @odoo-module */

import { PosDB } from "@point_of_sale/app/store/db";
import { unaccent } from "@web/core/utils/strings";
import { patch } from "@web/core/utils/patch";

patch(PosDB.prototype, {
    constructor(options) {
		this.get_orders_by_id = {};
		this.get_orders_by_barcode = {};
		this.get_orderline_by_id = {};
	},
});
