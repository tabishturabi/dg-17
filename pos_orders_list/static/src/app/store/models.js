/** @odoo-module */
import { Order } from "@point_of_sale/app/store/models";
import { patch } from "@web/core/utils/patch";

patch(Order.prototype, {
    setup(_defaultObj, options) {
        super.setup(...arguments);
        this.barcode = this.barcode || "";
        this.set_barcode();
    },

    set_barcode(){
        var self = this;    
        var temp = Math.floor(100000000000+ Math.random() * 9000000000000)
        self.barcode =  temp.toString();
    },

    export_as_JSON() {
        const json = super.export_as_JSON(...arguments);
        json.barcode = this.barcode;
        return json;
    },

    init_from_JSON(json){
        super.init_from_JSON(...arguments);
        this.barcode = json.barcode;
    },

    export_for_printing() {
        return {
            ...super.export_for_printing(...arguments),
            barcode: this.barcode,
        };
    },
});