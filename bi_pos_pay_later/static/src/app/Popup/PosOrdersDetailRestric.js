/** @odoo-module */

import { AbstractAwaitablePopup } from "@point_of_sale/app/popup/abstract_awaitable_popup";
import { _t } from "@web/core/l10n/translation";
import { usePos } from "@point_of_sale/app/store/pos_hook";
import { useService } from "@web/core/utils/hooks";

export class PosOrdersDetailRestric extends AbstractAwaitablePopup {
    static template = "bi_pos_pay_later.PosOrdersDetailRestric";
    

    setup() {
		super.setup();
		this.pos = usePos();
		this.popup = useService("popup");
	}

	async discard_order(){
		var order = this.pos.get_order()
		
		if(order.get_orderlines().length > 0 ){
			this.pos.removeOrder(order);
            this.pos.add_new_order()

            this.props.close({ confirmed: true, });
			await this.pos.showTempScreen('PosOrdersScreen', {
				'selected_partner_id': false,
				'filter_option':[],
			});
		}else{
			this.props.close({ confirmed: false, payload: null });
		}
	}
}