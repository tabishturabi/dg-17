/** @odoo-module */

import { PosDB } from "@point_of_sale/app/store/db";
import { unaccent } from "@web/core/utils/strings";
import { patch } from "@web/core/utils/patch";
import { PosStore } from "@point_of_sale/app/store/pos_store";
import { Orderline,Product, Order } from "@point_of_sale/app/store/models";
import { omit } from "@web/core/utils/objects";

patch(PosStore.prototype, {
    // @Override
    async _processData(loadedData) {
		await super._processData(...arguments);
		let self = this;
		this.pos_order = loadedData['pos_order'] || [];
	},
});


patch(Order.prototype, {
	setup(_defaultObj, options) {
        super.setup(...arguments);
        this.is_partial    = false;
		this.is_paying_partial    = false;
		this.amount_due    = 0;
		this.amount_paid    = 0;
		this.is_draft_order = false;
		this.set_is_partial();

    },

	set_is_partial(set_partial){
		this.is_partial = set_partial || false;
	},
	export_as_JSON(){
		const json = super.export_as_JSON(...arguments);
		json.is_partial = this.is_partial || false;
		json.amount_due = this.get_partial_due();
		json.is_paying_partial = this.is_paying_partial;
		json.is_draft_order = this.is_draft_order || false;
		return json;
	},
	init_from_JSON(json){
		super.init_from_JSON(...arguments);
		this.is_partial = json.is_partial;
		this.amount_due = json.amount_due;
		this.is_paying_partial = json.is_paying_partial;
		this.is_draft_order = json.is_draft_order;
	},
	get_partial_due(){
		let due = 0;
		if(this.get_due() > 0){
			due = this.get_due();
		}
		return due
	},

	export_for_printing(){
        const result = super.export_for_printing(...arguments);
        result.cust_order = this;

        let orderlines_list = []
        
        this.orderlines.forEach((line) => {
            if((this.is_paying_partial == false) || (this.is_paying_partial == true && line.price > 0)){
                orderlines_list.push(line);
            }
        });

        result['orderlines'] = orderlines_list.map((l) => omit(l.getDisplayData(), "internalNote"));
        return result
    },

});


patch(PosDB.prototype, {
    get_unpaid_orders(){
		var saved = this.load('unpaid_orders',[]);
		var orders = [];
		for (var i = 0; i < saved.length; i++) {
			let odr = saved[i].data;
			if(!odr.is_paying_partial && !odr.is_partial && !odr.is_draft_order){
				orders.push(saved[i].data);
			}
			if(odr.is_paying_partial || odr.is_partial || odr.is_draft_order){
				saved = saved.filter((o) => o.id !== odr.uid);
			}
		}
		this.save('unpaid_orders',saved);
		return orders;
	},
});