/** @odoo-module */

import { PaymentScreen } from "@point_of_sale/app/screens/payment_screen/payment_screen";
import { Product } from "@point_of_sale/app/store/models";
import { patch } from "@web/core/utils/patch";
import { useService } from "@web/core/utils/hooks";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup";
import { _t } from "@web/core/l10n/translation";
import { ConnectionLostError } from "@web/core/network/rpc_service";

patch(PaymentScreen.prototype, {

    setup() {
        super.setup()
        this.popup = useService("popup");
        this.orm = useService("orm");
        console.log("this----------------",this.pos.get_order())
    },

    async selectPartner() {
		let self = this;
		if (this.currentOrder.is_paying_partial){
			return self.popup.add(ErrorPopup, {
				title: _t('Not Allowed'),
				body: _t('You cannot change customer of draft order.'),
			});
		} else {
			super.selectPartner();
		}
	},

	remove_current_orderlines(){
		let self = this;
		let order = self.pos.get_order();
		let orderlines = order.get_orderlines();
		order.set_partner(null);           
        while (orderlines.length > 0) {
            orderlines.forEach(function (line) {
                order.removeOrderline(line);
            });
        }
        order.is_paying_partial=false
	},

	async click_back(){
		let self = this;
		if(this.currentOrder.is_paying_partial){
			const { confirmed } = await self.popup.add(ConfirmPopup, {
				title: _t('Cancel Payment ?'),
				body: _t('Are you sure,You want to Cancel this payment?'),
			});
			if (confirmed) {
				self.remove_current_orderlines();
				self.pos.showScreen('ProductScreen');
			}
		}
		else{
			self.pos.showScreen('ProductScreen');
		}
	},

	clickPayLater(){
		let self = this;
		let order = self.pos.get_order();
		let orderlines = order.get_orderlines();
		let partner_id = order.get_partner();
		if (!partner_id){
			return self.popup.add(ErrorPopup, {
				title: _t('Unknown customer'),
				body: _t('You cannot perform partial payment.Select customer first.'),
			});
		}
		else if(orderlines.length === 0){
			return self.popup.add(ErrorPopup, {
				title: _t('Empty Order'),
				body: _t('There must be at least one product in your order.'),
			});
		}
		else{

			order.is_partial = true;
			order.amount_due = order.get_due();
			order.set_is_partial(true);
			order.to_invoice = false;
			order.finalized = true;
			self.pos.push_single_order(order);
			self.pos.showScreen('ReceiptScreen');						
		}
	},
});