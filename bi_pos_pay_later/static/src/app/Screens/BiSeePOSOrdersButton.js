/** @odoo-module */

import { patch } from "@web/core/utils/patch";
import { SeePosOrderButton } from "@pos_orders_list/app/screens/product_screen/control_buttons/pos_order_button/pos_order_button";
import { Component, useEffect, useRef, onMounted } from "@odoo/owl";
import { usePos } from "@point_of_sale/app/store/pos_hook";
import { useService } from "@web/core/utils/hooks";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup"; 
import { PosOrdersDetailRestric } from "@bi_pos_pay_later/app/Popup/PosOrdersDetailRestric";


patch(SeePosOrderButton.prototype, {
    setup() {
		super.setup()
		this.pos = usePos();
		this.popup = useService("popup");
	},

	async click() {
		var order = this.pos.get_order();
		if(order.get_orderlines().length > 0){
			this.popup.add(PosOrdersDetailRestric,{
			})
		}else{
			await this.pos.showTempScreen('PosOrdersScreen', {
				'selected_partner_id': false ,
				'filter_option':[],
			});
		}
	}
});