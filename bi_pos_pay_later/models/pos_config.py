# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _ , tools
from odoo.exceptions import RedirectWarning, UserError, ValidationError


class POSConfigInherit(models.Model):
	_inherit = 'pos.config'
	
	allow_partical_payment = fields.Boolean('Allow Partial Payment')
	partial_product_id = fields.Many2one("product.product",string="Partial Payment Product", domain = [('type', '=', 'service'),('available_in_pos', '=', True)])


class ResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'


	allow_partical_payment = fields.Boolean(related='pos_config_id.allow_partical_payment',readonly=False)
	partial_product_id = fields.Many2one(related='pos_config_id.partial_product_id',readonly=False)

	@api.onchange('partial_product_id')
	def _onchange_partial_product_id(self):
		if self.partial_product_id:
			product = self.env['product.product'].browse(self.partial_product_id.id)
			if product.available_in_pos != True:
				raise ValidationError(_('Please enable available in POS for the Partial Payment Product'))

			if product.taxes_id:
				raise ValidationError(_('You are not allowed to add Customer Taxes in the Partial Payment Product'))