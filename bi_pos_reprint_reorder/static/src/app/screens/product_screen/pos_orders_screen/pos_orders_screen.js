/** @odoo-module */

import { patch } from "@web/core/utils/patch";
import { PosOrdersScreen } from "@pos_orders_list/app/screens/product_screen/pos_orders_screen/pos_orders_screen";
import { ReOrderPopup } from "@bi_pos_reprint_reorder/app/popup/reorder_popup";
import { Component, useEffect, useRef, onMounted } from "@odoo/owl";
import { usePos } from "@point_of_sale/app/store/pos_hook";
import { useService } from "@web/core/utils/hooks";

patch(PosOrdersScreen.prototype, {
    setup() {
        super.setup(...arguments);
        this.pos = usePos();
        this.orm = useService("orm");
        this.popup = useService("popup");
    },

    async clickReprint(order){
        let self = this;
        await this.orm.call(
            "pos.order",
            "print_pos_receipt",
            [[order.id]],
            ).then(function(output) {
                let data = output;
                data['order'] = order;
                self.pos.showScreen('OrderReprintScreen',data);
            }
        );
    },

    async clickReOrder(order){
        let self = this;
        let o_id = parseInt(order.id);
        let orderlines =  self.orderlines;              
        let pos_lines = [];

        for(let n=0; n < orderlines.length; n++){
            if (orderlines[n]['order_id'][0] ==o_id){
                pos_lines.push(orderlines[n])
            }
        }

        await this.popup.add(ReOrderPopup, { 'order': event.detail,'orderlines':pos_lines, });
    }
});
