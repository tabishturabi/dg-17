# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* bi_pos_reprint_reorder
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-22 06:41+0000\n"
"PO-Revision-Date: 2021-12-22 06:41+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Apply"
msgstr "Solicitar"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "CHANGE"
msgstr "CAMBIO"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Cancel"
msgstr "Cancelar"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Discount:"
msgstr "Descuento:"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Discounts"
msgstr "Descuentos."

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Logo"
msgstr "Logo"

#. module: bi_pos_reprint_reorder
#: model:ir.model,name:bi_pos_reprint_reorder.model_pos_order
msgid "Point of Sale Orders"
msgstr "Órdenes de punto de venta"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Print"
msgstr "Impresión"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Print Receipt"
msgstr "Imprimir el recibo"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Product"
msgstr "Producto"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Re-Order"
msgstr "Reordenar"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Receipt"
msgstr "Recibo"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Reorder Products"
msgstr "Reordenar Productos"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Reorder Qty"
msgstr "Reordenar Cant."

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Served by"
msgstr "Servido por"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "TOTAL"
msgstr "TOTAL"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Tel:"
msgstr "Tel:"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "Total Taxes"
msgstr "Impuestos totales"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "VAT:"
msgstr "IVA:"

#. module: bi_pos_reprint_reorder
#. openerp-web
#: code:addons/bi_pos_reprint_reorder/static/src/xml/pos_reprint_reorder.xml:0
#, python-format
msgid "back"
msgstr "espalda"
